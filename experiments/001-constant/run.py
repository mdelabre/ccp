from ccp.helper import dump_json
from ccp.simulator.runner import run
from ccp.tester.generator import generate
from datetime import datetime
import git
import itertools
import json
import os
import sys

if __name__ == '__main__':
    base_dir = os.path.join(
        'ccp',
        'experiments',
        '001-constant',
        'out',
        datetime.now().isoformat('T', 'minutes')
        if len(sys.argv) <= 1
        else sys.argv[1])

    try:
        os.makedirs(base_dir)
    except FileExistsError:
        print(f'Warning: “{base_dir}” already exists! \
Press enter to continue anyway.', end='')

        try:
            input()
        except KeyboardInterrupt:
            sys.exit(1)

    print(f'Output to {base_dir}')
    metadata_path = os.path.join(base_dir, 'metadata')

    try:
        with open(metadata_path, 'x') as metadata:
            repo = git.Repo(search_parent_directories=True)
            print(f'Head commit: {repo.head.object.hexsha}', file=metadata)
            print(f'Start time: {datetime.now()}', file=metadata)
    except FileExistsError:
        with open(metadata_path, 'a') as metadata:
            print(f'Resume time: {datetime.now()}', file=metadata)

    tδ = 10
    tp_vals = list(range(1, 10)) + list(range(10, 110, 10))
    te = 5
    mode_kp_vals = [('mape', 'inf')] \
        + list(itertools.product(('ccp',), range(1, 11)))
    dur = 10000
    max_elts = 20

    changes_path = os.path.join(base_dir, 'changes.json')

    try:
        with open(changes_path, 'x') as changes_file:
            print('Generating change sequence')

            def rotate_elt(elts):
                changes = []

                if len(elts) >= max_elts:
                    changes += [('remove', elts[0])]

                changes += [('add',)]
                return changes

            change_seq = generate({
                'sequence duration': dur,
                'event spacing gen': lambda: tδ,

                # Each event adds a new component and a new host, and removes
                # the oldest component and oldest host if there are more
                # than ``max_elts``, so that each execution phase takes exactly
                # tᴇ units of time
                'resource types': 1,

                'app initial size': 1,
                'component request gen': lambda: 1,
                'app action gen': rotate_elt,

                'infra initial size': 1,
                'host resource gen': lambda: 1,
                'infra action gen': rotate_elt,
            })

            changes_file.write(dump_json(change_seq))
    except FileExistsError:
        with open(changes_path, 'r') as changes_file:
            print('Using existing change sequence')
            change_seq = json.load(changes_file)

    for tp in tp_vals:
        for mode, kp in mode_kp_vals:
            simul_name = f'tp-{tp}-{mode}-kp-{kp}'
            log_path = os.path.join(base_dir, f'{simul_name}.log.jsonl')

            try:
                print(f'Simulating {simul_name}')

                with open(log_path, 'x') as log_file:
                    run({
                        'changes': change_seq,
                        'params': {
                            'placement algo': 'first fit',
                            'allowed migrations': 0,
                            'plan duration': tp,
                            'deployment duration': te,
                            'migration duration': te,
                            'undeployment duration': 0,
                            'plan concurrency': kp,
                            'MAPE-K mode': mode == 'mape',
                        },
                    }, log_file)
            except FileExistsError:
                print(f'Using existing {simul_name}')

    with open(metadata_path, 'a') as metadata:
        print(f'End time: {datetime.now()}', file=metadata)
