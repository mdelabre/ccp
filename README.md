# CCP

[See the package documentation →](https://mdelabre.gitlabpages.inria.fr/ccp/)

## Installation

_Prerequisite:_ A working Python ⩾ 3.6 install.

1. Clone this repository.

```sh
git clone https://gitlab.inria.fr/mdelabre/ccp.git
```

2. Create a virtual Python environment to install dependencies.

```sh
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

3. Install the `ortools` package.

```sh
pip install ortools
```

4. Run an example configuration to ensure everything is working.

```sh
./simulate examples/basic.json
```

5. When you’re done, you can unload the virtual environment.

```sh
deactivate
```
