"""Common CCP utilities."""

import argparse
import json
import sys


class HelpFormatter(
        argparse.RawDescriptionHelpFormatter,
        argparse.ArgumentDefaultsHelpFormatter):
    """Help formatter that does not reflow text and shows default values."""

    def _get_help_string(self, action):
        help = action.help
        if '%(default)' not in action.help:
            if action.default not in [None, argparse.SUPPRESS]:
                defaulting_nargs = [argparse.OPTIONAL, argparse.ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' (default: %(default)s)'
        return help


def create_arg_parser(module):
    parser = argparse.ArgumentParser(
        description=sys.modules[module].__doc__,
        formatter_class=HelpFormatter)

    parser._actions[0].option_strings = ['--help', '-h']
    return parser


def dump_json(obj):
    return json.dumps(obj, indent=2)


def update(target, src):
    """
    Merge a set of values in a dictionary.

    This is a shallow merge.

    :param dict target: Dictionary that will receive the new values.
    :param dict src: New values to assign to the target.
    :return: Return the target dictionary after update.
    """
    target.update(src)
    return target


def choose_option(rg, options):
    """
    Select an option randomly among weighted choices.

    :param rg: Random generator state.
    :param List[float] options: List of options, each one being weighted by
        a floating point number from 0 to 1. This weight represents the
        probability of selecting the given option. When the sum of all
        weights is lower than 1, the remaining probability is assigned to
        an out-of-bounds option.
    :return: The index of a randomly selected option, or an out-of-bounds
        option if the sum does not add up to 1 and none of the existing options
        were chosen.
    """
    indicator = rg.random()
    summed = 0

    for i in range(len(options)):
        summed += options[i]

        if indicator < summed:
            return i

    return len(options)


def random_element(rg, options):
    """
    Select a random element uniformly from a list of options.

    :param rg: Random generator state.
    :param list options: List of options.
    :return: The value of a randomly selected option.
    """
    return options[rg.randint(len(options))]
