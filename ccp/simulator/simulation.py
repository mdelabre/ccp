# noqa
import simpy
import json


class Simulation:
    """
    State of a simulation.

    :var simpy.Environment env: Reference to a SimPy simulation engine.
    :var Dict[str,List[int]] app: Set of application components.
    :var Dict[str,List[int]] infra: Set of infrastructure hosts.
    :var Dict[str,str] placement: Placement goal of components onto hosts.
    :var Dict[str,Deployment] deployment: Deployment state of each component.
    :var simpy.Resource plan_semaphore: Semaphore for limiting the number of
        concurrent *Plan* phases.
    :var simpy.Resource loop_semaphore: Semaphore for limiting the number of
        concurrent reaction processes, i.e. *Plan*/*Execute* pairs. This is
        mainly used for simulating a MAPE-K loop (with no concurrency) by
        setting the semaphore capacity to 1. When simulating the CCP model,
        this semaphore is a no-op.
    :var dict params: Simulation parameters.
    """

    def __init__(self, env, output, params={}):
        """
        Create an object holding the state of a CCP simulation.

        :param simpy.Environment env: SimPy simulation engine.
        :param file output: Output file to which the execution log will be
            written.
        :param dict params: (optional, defaults to `{}`)
            Simulation parameters.
        :param str params['placement algo']: (optional, defaults to
            `'mixed integer prog'`) Algorithm to use for placing components
            on hosts. Choices are `'mixed integer prog'` (see
            :py:func:`.placements.mixed_integer_prog`), which uses the CBC
            mixed-integer linear programming solver to find optimal placements;
            and `'first fit'` (see :py:func:`.placements.first_fit`), a fast
            greedy algorithm.
        :param int|str params['allowed migrations']: (optional, defaults
            to 0) Number of nodes that are allowed to be migrated at each
            plan phase. Set to 0 to disallow migration, or to `'inf'` to
            allow them without limit.
        :param int params['plan duration']: (optional, defaults to `5`)
            Total time needed for computing a new deployment plan.
        :param int params['deployment duration']: (optional, defaults to `5`)
            Total time needed for deploying an undeployed component.
        :param int params['migration duration']: (optional, defaults to `10`)
            Total time needed for migrating an already-deployed component.
        :param int params['undeployment duration']: (optional, defaults to
            `10`) Total time needed for removing a deployed component.
        :param int|str params['plan concurrency']: (optional, defaults to
            `'inf'`) Maximum number of concurrent plan phases. Set to
            `'inf'` to disable any limitation.
        :param bool params['MAPE-K mode']: (optional, defaults to `False`)
            Whether to simulate a MAPE-K loop by disabling any concurrency
            between loop iterations.
        """
        self.env = env
        self.output = output

        self.app = {}
        self.infra = {}
        self.placement = {}
        self.deployment = {}

        plan_concurrency = (
            params['plan concurrency']
            if params.get('plan concurrency', 'inf') != 'inf'
            else float('inf'))
        self.plan_sem = simpy.Resource(env, capacity=plan_concurrency)

        loop_concurrency = (
            1 if params.get('MAPE-K mode', False)
            else float('inf'))
        self.loop_sem = simpy.Resource(env, capacity=loop_concurrency)

        self.params = {
            'placement algo': 'mixed integer prog',
            'allowed migrations': 0,
            'plan duration': 5,
            'deployment duration': 5,
            'migration duration': 10,
            'undeployment duration': 10,
        }
        self.params.update(params)

    def schedule(self, changes):
        """Schedule a set of changes into the simulation."""
        for change in changes:
            self.env.process(change.schedule())

    def _log(self, kind, value):
        """Log an event."""
        print(json.dumps({
            'time': self.env.now,
            'kind': kind,
            'value': value,
        }), file=self.output)
