from .simulation import Simulation
from .change import Change
from simpy import Environment


def run(instance, output):
    """
    Run a simulation and create an event log.

    :param dict instance: Description of the instance to run, which may be
        loaded from a JSON file.
    :param list instance['changes']: List of changes that will occur in the
        run. Each change must be described following the format specified
        in :py:meth:`.change.Change.__init__`.
    :param dict instance['params']: Simulation parameters to use for this
        run. See :py:meth:`.simulation.Simulation.__init__`
        for a list of available parameters.
    :param file output: Output file to which the execution log will be
        written.

    .. seealso::
        The :ref:`scripts-simulate` script can be used to load an instance
        description, run it and send the execution log to the standard output.
    """
    env = Environment()
    simul = Simulation(env, output, instance.get('params', {}))
    simul.schedule(
        Change(simul, change)
        for change in instance.get('changes', []))

    env.run()
