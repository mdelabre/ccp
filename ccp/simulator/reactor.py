# noqa
import simpy
from . import placements
from typing import Dict, Optional
from uuid import uuid4


def _merge_placement(sim, new_placement):
    merged_placement = {}

    for comp in sim.app.keys():
        if comp in new_placement and new_placement[comp] in sim.infra:
            merged_placement[comp] = new_placement[comp]
        elif comp in sim.placement:
            merged_placement[comp] = sim.placement[comp]

    # Check that each host’s resources are still not exceeded
    total_req = {
        host: [0 for res in sim.infra[host]]
        for host in sim.infra.keys()
    }

    for comp, host in merged_placement.items():
        total_req[host] = [
            sum(x) for x in
            zip(total_req[host], sim.app[comp])
        ]

    is_valid_req = all(
        all(req <= res for req, res in zip(total_req[host], sim.infra[host]))
        for host in sim.infra.keys())

    # Check that the number of migrations is still within the allowed range
    if sim.params['allowed migrations'] == 'inf':
        is_valid_mg = True
    else:
        migrations = sum(
            sim.deployment[comp].would_migrate(merged_placement[comp])
            for comp in sim.app.keys()
            if comp in merged_placement)

        is_valid_mg = migrations <= sim.params['allowed migrations']

    return merged_placement if is_valid_req and is_valid_mg else None


def plan(id, sim) -> Optional[Dict[str, str]]:
    """
    Compute a new placement following app or infra state changes.

    If the number of currently running *Plan* phases is already equal to the
    `'plan concurrency'` limit, this process will queue itself and wait for the
    next available slot. Note that *Plan* phases can be preempted, i.e. if a
    new *Plan* phase queues itself after this one (because other changes happen
    in the meantime), this one will be aborted and priority will be given to
    the next one. In this case, no events are logged.

    When running, the algorithm requested by the `'placement algo'` simulation
    parameter will be used. These algorithms can fail, in which case an
    `error` event will be logged.

    It is important to note that this placement algorithm is run on the state
    of the simulation at the beginning of the *Plan* phase, which can be
    substantially different from the state at the end of the *Plan* phase
    (i.e. after waiting `'plan duration'` units of time).

    At the end of the phase, a merge algorithm is run which tries to reconcile
    the new simulation state with the computed placement. If this merge fails
    (because the new state is too far removed from the old one), a `cancel`
    event will be logged.

    :param str id: Unique identifier for the parent reaction process.
    :param Simulation sim: Parent CCP simulation.
    :return: The newly computed placement, or `None` if the placement algorithm
        failed.
    """
    # If too many plan phases are already running, wait until they finish
    with sim.plan_sem.request() as req:
        yield req

        # If preempted by other changes, abort
        if sim.plan_sem.queue:
            return None

        sim._log('reaction', {'id': id, 'phase': 'plan', 'kind': 'start'})
        algo_name = sim.params['placement algo'].replace(' ', '_')
        placement_algo = getattr(placements, algo_name)
        new_placement = placement_algo(sim)

        if new_placement is None:
            sim._log('reaction', {'id': id, 'phase': 'plan', 'kind': 'error'})
            return None

        yield sim.env.timeout(sim.params['plan duration'])
        merged_placement = _merge_placement(sim, new_placement)

        if merged_placement is None:
            sim._log('reaction', {'id': id, 'phase': 'plan',
                                  'kind': 'cancel'})
            return None

        sim._log('reaction', {'id': id, 'phase': 'plan', 'kind': 'end',
                              'placement': merged_placement})
        return merged_placement


def execute(id, sim):
    """
    Execute the new placement plan by changing deployments of components.

    :param str id: Unique identifier for the parent reaction process.
    :param Simulation sim: Parent CCP simulation.

    .. seealso::
        The :py:meth:`.deployment.Deployment.deploy` method for a full
        description of the process of deploying each component.
    """
    sim._log('reaction', {'id': id, 'phase': 'execute', 'kind': 'start'})

    yield simpy.events.AllOf(sim.env, [
        sim.env.process(sim.deployment[comp].deploy(host))
        for comp, host in sim.placement.items()
    ])

    sim._log('reaction', {'id': id, 'phase': 'execute', 'kind': 'end'})


def react(sim):
    """
    Start a reaction process following a change in the app or infra.

    When simulating a MAPE-K loop, this process will queue itself to run
    after the current loop iteration if there is already one running. In this
    case, preemption is possible, i.e. if a new process tries to queue itself
    after this one, priority will be given to it.

    The reaction process is the sequence of a *Plan* phase
    (see :py:func:`.plan`) and an *Execute* phase (see :py:func:`.execute`).
    When either phase starts and ends, corresponding `start` and `end` events
    that share a common unique ID are logged.

    :param Simulation sim: Parent CCP simulation.
    """
    id = str(uuid4())

    # If simulating a MAPE-K loop, wait until other processes have finished
    with sim.loop_sem.request() as req:
        yield req

        # If preempted by other changes, abort
        if sim.loop_sem.queue:
            return

        # Request the computation of a new placement
        plan_process = sim.env.process(plan(id, sim))
        yield plan_process

        if plan_process.value is None:
            return

        sim.placement = plan_process.value

        # Execute the newly computed placement
        yield sim.env.process(execute(id, sim))
