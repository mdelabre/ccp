# noqa
from .deployment import Deployment
from .reactor import react
from uuid import uuid4


class Change():
    """Change in the simulated app or infra."""

    def __init__(self, sim, desc):
        """
        Create a change in the simulated app or infra.

        :param Simulation sim: Parent CCP simulation.
        :param dict desc: Change description.
        :param int desc['time']: Time point at which the change will happen.

        There are two mutually exclusive possible sets of keys to describe the
        actual change in the `desc` argument:

        **(Set 1.) Full replace:** As a new set of components and hosts.

        :param dict desc['new app']: New set of application components.
            This is a dictionary where keys are identifiers for each component
            and values are lists of resources requested by each component.
        :param dict desc['new infra']: New set of infrastructure hosts.
            This is also a dictionary where keys are identifiers for each
            host and values are lists of resources available on each one.

        **(Set 2.) Delta:** As a diff relative to the existing state.

        :param dict desc['delta app']: Differences to the existing set of
            components. Keys that did not exist will create new components,
            existing keys will replace existing components, and keys with a
            value of `'remove'` will remove the component they designate.
        :param dict desc['delta infra']: Differences to the existing set of
            hosts. Keys that did not exist will create new hosts, existing keys
            will replace existing hosts, and keys with a value of `'remove'`
            will remove the hosts they designate.
        """
        self.sim = sim
        self.time = desc['time']

        if 'new app' in desc or 'new infra' in desc:
            self.new_app = desc.get('new app', {})
            self.new_infra = desc.get('new infra', {})
            self.delta_app = None
            self.delta_infra = None
        else:
            self.new_app = None
            self.new_infra = None
            self.delta_app = desc.get('delta app', {})
            self.delta_infra = desc.get('delta infra', {})

    def schedule(self):
        """Schedule this change into the parent simulation."""
        yield self.sim.env.timeout(self.time)

        # Update the app and infra state
        if self.new_app is not None or self.new_infra is not None:
            self.sim.app = self.new_app.copy()
            self.sim.infra = self.new_infra.copy()
        else:
            for target, delta in ((self.sim.app, self.delta_app),
                                  (self.sim.infra, self.delta_infra)):
                for key, value in delta.items():
                    if value == 'remove':
                        del target[key]
                    else:
                        target[key] = value

        # Update deployment states
        for comp in self.sim.app.keys():
            if comp not in self.sim.deployment:
                # Initialize new components to “undeployed” state
                self.sim.deployment[comp] = Deployment(self.sim, comp)

        for comp, state in list(self.sim.deployment.items()):
            if comp not in self.sim.app:
                # Delete components that were removed
                del self.sim.deployment[comp]
            elif (state.to_host is not None and state.to_host
                  not in self.sim.infra) \
                    or (state.host is not None and state.host
                        not in self.sim.infra):
                # Undeploy components whose host has been removed
                # (launch in parallel)
                self.sim.env.process(state.deploy(None))

        # Update placement
        for comp, host in list(self.sim.placement.items()):
            if comp not in self.sim.app or host not in self.sim.infra:
                del self.sim.placement[comp]

        self.sim._log('change', {
            'id': str(uuid4()),
            'app': self.sim.app,
            'infra': self.sim.infra,
        })

        # React to the change
        yield self.sim.env.process(react(self.sim))
