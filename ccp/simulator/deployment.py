# noqa


class Deployment():
    """
    Deployment state of a component.

    A component can be in one of these five valid deployment states:

    * *Undeployed* (Ud): If it is not deployed on any host.
    * *Deploying* (Dg): If it was previously undeployed but is now being
        deployed to the target host :py:attr:`to_host`.
    * *Deployed* (Dd): If it is deployed and available on its target host
        :py:attr:`host`.
    * *Migrating* (Mg): If it has changed target and is currently being
        migrated from :py:attr:`from_host` to :py:attr:`to_host`.
    * *Undeploying* (Ug): If it is being removed from its previous target
        host :py:attr:`from_host`.

    During the *Deploying*, *Migrating*, or *Undeploying* states, the
    :py:attr:`transition_done` event variable exists and is processed when the
    component transitions back to a “stable” state.
    """

    def __init__(self, sim, comp):
        """
        Initialize the deployment state of a component to “undeployed”.

        :param Simulation sim: Parent CCP simulation.
        :param str comp: Name of the associated component.
        """
        self.sim = sim
        self.comp = comp
        self.transition_done = None

        self.host = None
        self.from_host = None
        self.to_host = None

    def is_undeployed(self):
        """Check if a component is currently undeployed."""
        return self.host is None \
            and self.from_host is None \
            and self.to_host is None

    def is_deploying(self):
        """Check if a component is currently being deployed."""
        return self.host is None \
            and self.from_host is None \
            and self.to_host is not None

    def is_deployed(self):
        """Check if a component is currently deployed."""
        return self.host is not None \
            and self.from_host is None \
            and self.to_host is None

    def is_migrating(self):
        """Check if a component is currently being migrated."""
        return self.host is None \
            and self.from_host is not None \
            and self.to_host is not None

    def is_undeploying(self):
        """Check if a component is currently being undeployed."""
        return self.host is None \
            and self.from_host is not None \
            and self.to_host is None

    def state(self):  # noqa
        if self.is_undeployed():
            return {'status': 'Ud'}

        if self.is_deploying():
            return {'status': 'Dg', 'to': self.to_host}

        if self.is_deployed():
            return {'status': 'Dd', 'on': self.host}

        if self.is_migrating():
            return {'status': 'Mg', 'from': self.from_host, 'to': self.to_host}

        if self.is_undeploying():
            return {'status': 'Ug', 'from': self.from_host}

        return None

    def would_migrate(self, target):
        """
        Check if deploying the component on a host would cause a migration.

        This is true if the component is already deployed on, being deployed
        to, or being migrated to a different host from `target`.

        :param str target: Target host to check.
        """
        if self.is_deployed() and self.host != target:
            return True

        if self.is_deploying() and self.to_host != target:
            return True

        if self.is_migrating() and self.to_host != target:
            return True

        return False

    def deploy(self, target):
        """
        Initiate the deployment, migration or undeployment of this component.

        :param Optional[str] target: Target host for this component’s
            deployment, or None to trigger undeployment.

        The component state is modified so that, eventually, it is deployed on
        the `target` host. If `target` is `None`, it means the component must
        be undeployed.

        If the component is already going through a transition, its completion
        is waited before any action. If the target host is already the one
        on which the component is deployed, nothing is done.

        Depending on the actual action performed, a total of `'deployment
        duration'`, `'migration duration'` or `'undeployment duration'` units
        of time will be waited before changing the state of the component.
        """
        if self.is_deploying() or self.is_migrating() or self.is_undeploying():
            # Wait until the currently active transition is finished
            yield self.transition_done

        if target != self.host:
            if self.is_undeployed():
                # Initiate deployment of an undeployed component
                self.to_host = target
                self._log()

                duration = self.sim.params['deployment duration']
                self.transition_done = self.sim.env.timeout(duration)
                yield self.transition_done

                self.host = self.to_host
                self.to_host = None
                self._log()
            elif self.is_deployed():
                # Initiate migration/undeployment of a deployed component
                self.to_host = target
                self.from_host = self.host
                self.host = None
                self._log()

                duration = self.sim.params['migration duration'] \
                    if target is not None \
                    else self.sim.params['undeployment duration']
                self.transition_done = self.sim.env.timeout(duration)
                yield self.transition_done

                self.host = self.to_host
                self.from_host = None
                self.to_host = None
                self._log()

    def _log(self):
        self.sim._log('deployment', {
            'component': self.comp,
            **self.state(),
        })

    def __str__(self):  # noqa
        if self.is_undeployed():
            return 'Ud'
        elif self.is_deploying():
            return f'Dg(ε → {self.to_host})'
        elif self.is_deployed():
            return f'Dd({self.host})'
        elif self.is_migrating():
            return f'Mg({self.from_host} → {self.to_host})'
        elif self.is_undeploying():
            return f'Ug({self.from_host} → ε)'
        else:
            return '[Invalid state]'
