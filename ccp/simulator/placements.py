"""Placement algorithms."""


def mixed_integer_prog(sim):
    """
    Compute an optimal placement using a mixed-integer LP formulation.

    The following constants (set in italics) are inputs of the problem:

    .. list-table::
        :widths: 20 80

        * - :math:`n`
          - The number of components in the infrastructure.
        * - :math:`m`
          - The number of hosts in the infrastructure.
        * - :math:`r`
          - The number of resource types to consider.
        * - :math:`\\mathit{req}_i`
          - The resources needed by the |ith| component (as an array of
            :math:`r` numbers, each of which is the quantity of the requested
            resource).
        * - :math:`\\mathit{res}_j`
          - The resources available on the |jth| host (as an array of :math:`r`
            numbers, each of which is the quantity of the available resource).
        * - :math:`\\mathit{no\\_migration}_{i,j}`
          - A boolean indicator of whether assigning the |jth| host to the
            |ith| component would not trigger a migration. This is the negation
            of the result returned by
            :py:meth:`.deployment.Deployment.would_migrate`.
        * - :math:`\\mu`
          - The number of allowed migrations (which is the value of the
            `'allowed migrations'` simulation parameter).

    In this description, :math:`i` is usually the index of a specific
    component, :math:`j` is usually the index of a specific host, and :math:`k`
    is usually the index of a specific resource type.

    The following boolean variables (set in roman) are to be optimized.

    .. list-table::
        :widths: 20 80

        * - |deployij|
          - True when the |ith| component is assigned to the |jth| host.
        * - :math:`\\mathrm{migrate}_i`
          - True when the value of |deployij| will cause a migration of the
            |ith| component.
        * - :math:`\\mathrm{used}_j`
          - True when at least one component is assigned to the |jth| host.

    .. |ith| replace:: *i*\\ :sup:`th`
    .. |jth| replace:: *j*\\ :sup:`th`
    .. |deployij| replace:: :math:`\\mathrm{deploy}_{i,j}`

    Using those variables, the mixed-integer LP formulation is as follows.
    The :math:`[1..n]` notation is used here to refer to the
    :math:`\\{1, ..., n\\}` set.

    .. math::

        \\min \\quad & \\sum_{i=1}^{n}{\\mathrm{migrate}_i} +
            \\sum_{j=1}^{m}{\\mathrm{used}_j} \\\\[1em]
        \\text{subject to} \\quad
            & \\sum_{j=1}^{m}{\\mathrm{deploy}_{i,j}} = 1
                \\quad \\forall i \\in [1..n]\\\\[1em]
            & \\mathrm{migrate}_i \\geq \\mathrm{deploy}_{i,j}
                - \\mathit{no\\_migration}_{i,j}
                \\quad \\forall i \\in [1..n]
                       \\forall j \\in [1..m]\\\\[1em]
            & \\sum_{i=1}^{n}{
                \\mathrm{deploy}_{i,j} \\times \\mathit{req}_i[k]} \\leq
                \\mathrm{used}_j \\times \\mathit{res}_j[k]
                \\quad \\forall j \\in [1..m]
                       \\forall k \\in [1..r]\\\\[1em]
            & \\sum_{i=1}^{n}{\\mathrm{migrate}_i} \\leq \\mu

    This formulation strives to minimize the number of used hosts. The first
    constraint makes sure that each component is assigned to exactly one host.
    The second, combined with the :math:`\\sum_{i=1}^{n}{\\mathrm{migrate}_i}`
    term in the objective function, forces the :math:`\\mathrm{migrate}_i`
    variables to take their appropriate value. The third prevents cases where
    the resources of a host are exceeded by the components that are assigned to
    it.  Finally, the fourth constraint invalidates solution that use more
    migrations that allowed (only when the `'allowed migrations'` simulation
    parameter is not :math:`\\infty`).

    To find solutions to this mixed-integer linear programming problem, the
    `OR-Tools <https://developers.google.com/optimization/>`_ Python interface
    to the `CBC <https://github.com/coin-or/Cbc>`_ solver is used.

    :param Simulation sim: Parent CCP simulation:
    :return: The computed placement if the problem is solvable, or None
        otherwise.
    """
    from ortools.linear_solver import pywraplp

    if not sim.app or not sim.infra:
        # Empty placement for empty application or infrastructure
        return {}

    # Compute a new placement based on the existing deployment state and
    # on available resources
    program = pywraplp.Solver(
        'Component Placement Problem',
        pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)

    # Number of components
    components = list(sim.app.keys())
    n = len(components)

    # Number of hosts
    hosts = list(sim.infra.keys())
    m = len(hosts)

    # Number of resource types
    r = len(next(iter(sim.app.values())))

    # deploy[i][j]: true iff component i is to be deployed on host j
    deploy = [[program.BoolVar(f'deploy[{i}][{j}]') for j in range(m)]
              for i in range(n)]

    # migrate[i]: true iff the assignment causes a migration of comp i
    migrate = [program.BoolVar(f'migrate[{i}]') for i in range(n)]

    # used[j]: true iff host j is used by at least one component
    used = [program.BoolVar(f'used[{j}]') for j in range(m)]

    # Each component should be deployed on one, and only one, host
    for i in range(n):
        program.Add(program.Sum([deploy[i][j] for j in range(m)]) == 1)

    # Detect any migration that will be triggered
    for i in range(n):
        for j in range(m):
            no_migration = 1 - int(sim.deployment[components[i]]
                                   .would_migrate(hosts[j]))
            program.Add(migrate[i] >= deploy[i][j] - no_migration)

    # Do not exceed the resources of each host
    for j in range(m):
        for k in range(r):
            program.Add(
                program.Sum([
                    deploy[i][j] * sim.app[components[i]][k]
                    for i in range(n)
                ]) <= used[j] * sim.infra[hosts[j]][k])

    # Do not exceed the maximum number of allowed migrations
    if sim.params['allowed migrations'] != 'inf':
        program.Add(program.Sum(migrate) <= sim.params['allowed migrations'])

    # Objective function
    program.Minimize(program.Sum(migrate) + program.Sum(used))

    # Try to solve and extract solution
    status = program.Solve()

    if status == pywraplp.Solver.OPTIMAL:
        placement = {}

        for i in range(n):
            for j in range(m):
                if deploy[i][j].solution_value() >= 0.5:
                    placement[components[i]] = hosts[j]

        return placement

    return None


def first_fit(sim):
    """
    Compute a placement using a fast, non-optimal, greedy heuristic.

    The heuristic implemented in this algorithm consists in reusing the
    existing placement and iteratively assigning each unassigned component
    to the first host that has enough resources.

    Solutions provided by this algorithm always satisfy the constraints (in
    particular, they never trigger any migration). However, they are most of
    the time non-optimal. Moreover, because the heuristic does not strive to
    use the full resources available, it can fail to find a solution more often
    than exact approaches.

    :param Simulation sim: Parent CCP simulation:
    :return: The computed placement if the problem is solvable, or None
        otherwise.
    """
    # Start with the existing placement
    placement = sim.placement.copy()

    # Compute remaining resources for each host
    types = len(next(iter(sim.app.values())))

    remaining_res = {
        host: sim.infra[host].copy()
        for host in sim.infra.keys()
    }

    for comp, host in placement.items():
        for i in range(types):
            remaining_res[host][i] -= sim.app[comp][i]

    # Assign each component to the first available host
    hosts = list(sim.infra)
    cur_host_idx = 0

    if not hosts:
        # No available host
        return None

    for cur_comp in sim.app:
        if cur_comp not in placement:
            # Find the next available host (potentially the current one)
            while not all(remaining_res[hosts[cur_host_idx]][i]
                          >= sim.app[cur_comp][i]
                          for i in range(types)):
                if cur_host_idx + 1 < len(hosts):
                    cur_host_idx += 1
                else:
                    # No remaining host
                    return None

            placement[cur_comp] = hosts[cur_host_idx]

            for i in range(types):
                remaining_res[hosts[cur_host_idx]][i] \
                    -= sim.app[cur_comp][i]

    return placement
