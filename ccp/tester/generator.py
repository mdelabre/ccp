from .. import helper
from typing import Callable, Any


def make_random_var(rg, rv, postprocess=lambda x: x) -> Callable[[], Any]:
    """
    Adapt a SciPy distribution instance into a nullary callable.

    :param rg: Random generator state to bind to the variable.
    :param rv: SciPy distribution instance.
    :param postprocess: (optional, defaults to the identity function)
        Function to apply to each generated value.
    :return: A nullary callable that generates a random value following `rv`.
    """
    rv.random_state = rg
    return lambda: postprocess(rv.rvs())


def make_random_action_generator(rg, count, add_prob, remove_prob):
    """
    Create a random app/infra action generator.

    Action generators are suitable for use as the `'app action gen'` or
    `'infra action gen'` keys of the `generate` procedure and define
    the nature of each event that happens in a change sequence.

    :param count: Number of actions in each sequence.
    :param add_prob: Probability that each action is an addition.
    :param remove_prob: Probability that each action is a removal.

    Each action can either be the addition of a new element, the removal
    of one of the existing elements (if one exists) chosen uniformly at random
    or a no-op. The probability for a no-op is
    :math:`1 - \\mathit{add\\_prob} - \\mathit{remove\\_prob}`.

    :return: Action generator that suits the requested specification.
    """
    def action_generator(elements):
        actions = []

        for i in range(count):
            option = helper.choose_option(rg, [
                add_prob,
                remove_prob if elements else 0
            ])

            if option == 0:
                # Add a new element
                actions.append(('add',))
            elif option == 1:
                # Remove one of the existing elements
                actions.append(('remove', helper.random_element(rg, elements)))
            else:
                # Do nothing
                pass

        return actions

    return action_generator


class State:
    def __init__(self, params):
        self.res_types = params['resource types']
        self.comp_req = params['component request gen']
        self.host_res = params['host resource gen']

        self.app = {}
        self.next_comp_id = 1

        for i in range(params['app initial size']):
            self.app_add()

        self.infra = {}
        self.next_host_id = 1

        for i in range(params['infra initial size']):
            self.infra_add()

    def app_add(self):
        comp = f'C{self.next_comp_id}'
        self.next_comp_id += 1
        self.app[comp] = [self.comp_req() for i in range(self.res_types)]
        return comp

    def infra_add(self):
        host = f'H{self.next_host_id}'
        self.next_host_id += 1
        self.infra[host] = [self.host_res() for i in range(self.res_types)]
        return host


def generate(params={}):
    """
    Generate a sequence of changes to an app and infra state.

    :param dict params: (optional, defaults to `{}`) Generation parameters.
    :param int params['sequence duration']: (optional, defaults to `100`)
        Number of time units that the sequence needs to span.
    :param Callable[[],int] params['event spacing gen']: (optional, defaults
        to a constant spacing of `25` time units) A function which is called
        repeatedly to generate the number of time units which separates any two
        change events. This may for example be a constant or a number drawn
        from a probability distribution. See :py:func:`.make_random_var` for
        adapting SciPy random distributions into generating functions.
    :param int params['resource types']: (optional, defaults to `3`)
        Number of different kinds of resources that may be provided by hosts
        and requested by components.
    :param int params['app initial size']: (optional, defaults to `5`)
        Number of components in the application at the beginning.
    :param Callable[[],int] params['component request gen']: (optional,
        defaults to a constant request of `1`) A function which is called
        every time a new component is created to generate the amount of each
        resource type that this new component will request.
    :param Callable[[List[str]],List[Tuple]] params['app action gen']:
        (optional, defaults to adding a new component every time) A function
        which is called upon each change event to decide what to do to the app
        during that change. It is passed the list of currently existing
        components (before the change), and must return a list of tuples which
        can be of the two following forms:

        .. list-table::
            :widths: 40 60

            * - `('add',)`
              - Add a new component to the application.
            * - `('remove', 'id')`
              - Remove the component with ID `'id'` from the application.

        See :py:func:`.make_random_action_generator` which can create
        action-generating functions that produce random changes according to
        specified probabilities.
    :param int params['infra initial size']: (optional, defaults to `5`)
        Number of hosts in the infrastructure at the beginning.
    :param Callable[[],int] params['host resource gen']: (optional,
        defaults to a constant value of `10`) A function which is called
        every time a new host is created to generate the amount of each
        resource type that this new host will provide.
    :param Callable[[List[str]],List[Tuple]] params['infra action gen']:
        (optional, defaults to a no-op) A function which is called upon each
        change event to decide what to do to the infra during that change. It
        is passed the list of currently existing hosts (before the
        change), and must return a list of tuples which can be of the two
        following forms:

        .. list-table::
            :widths: 40 60

            * - `('add',)`
              - Add a new host to the infrastructure.
            * - `('remove', 'id')`
              - Remove the host with ID `'id'` from the infrastructure.
    :return: Sequence of change objects which can be passed to the
        `instance['changes']` parameter of :py:func:`.runner.run`.
    """
    params = helper.update({
        'sequence duration': 100,
        'event spacing gen': lambda: 25,

        'resource types': 3,

        'app initial size': 5,
        'component request gen': lambda: 1,
        'app action gen': lambda: [('add',)],

        'infra initial size': 5,
        'host resource gen': lambda: 10,
        'infra action gen': lambda: [],
    }, params)

    state = State(params)

    changes = [{
        'time': 0,
        'new app': state.app.copy(),
        'new infra': state.infra.copy(),
    }]

    time = 0

    while True:
        time += params['event spacing gen']()

        if time >= params['sequence duration']:
            break

        delta_app = {}
        delta_infra = {}

        for kind, struct, delta in (('app', state.app, delta_app),
                                    ('infra', state.infra, delta_infra)):
            for action in params[f'{kind} action gen'](list(struct)):
                if action[0] == 'add':
                    elt = getattr(state, f'{kind}_add')()
                    delta[elt] = struct[elt]
                else:
                    delta[action[1]] = 'remove'
                    del struct[action[1]]

        if delta_app or delta_infra:
            change = {'time': time}

            if delta_app:
                change['delta app'] = delta_app

            if delta_infra:
                change['delta infra'] = delta_infra

            changes.append(change)

    return changes
