import json
from typing import Dict, List


def measure(logfile) -> Dict[str, List[int]]:
    """
    Extract metrics from the event log produced by a simulation run.

    :param file logfile: Input stream of events as produced by a simulation
        run. See :ref:`event-log-output` for the format that this stream
        should follow.
    :return: A dictionary containing the two metrics extracted for the time
        being:

        * in `'response'`, the list of response times, which are the times
          between each change event and the start of the reaction process that
          handled it successfully;
        * in `'execution'`, the list of execution times, which are the times
          between each change and the end of the reaction process that handled
          it successfully.
    """
    measures = {
        'response': [],
        'execution': [],
    }

    # Set of events which were not fully processed (i.e. did not go through a
    # finished reaction process) yet
    pending_changes = {}

    # Events associated to each reaction process
    reaction_events = {}

    # Starting time of each reaction process
    reaction_start = {}

    for line in logfile:
        event = json.loads(line)
        time = event['time']

        if event['kind'] == 'change':
            change_id = event['value']['id']
            pending_changes[change_id] = time

        elif event['kind'] == 'reaction':
            reaction = event['value']
            reaction_id = reaction['id']

            if reaction['phase'] == 'plan':
                if reaction['kind'] == 'start':
                    reaction_start[reaction_id] = time
                    reaction_events[reaction_id] = list(pending_changes)

                elif reaction['kind'] in ('error', 'cancel'):
                    del reaction_start[reaction_id]
                    del reaction_events[reaction_id]

            elif reaction['phase'] == 'execute' and reaction['kind'] == 'end':
                for change_id in reaction_events[reaction_id]:
                    if change_id in pending_changes:
                        measures['response'].append(
                            reaction_start[reaction_id]
                            - pending_changes[change_id])

                        measures['execution'].append(
                            time - pending_changes[change_id])

                        del pending_changes[change_id]

                del reaction_start[reaction_id]
                del reaction_events[reaction_id]

    return measures
