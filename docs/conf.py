# Configuration file for the Sphinx documentation builder.
# See <https://www.sphinx-doc.org/en/master/usage/configuration.html>

import inspect

# -- Path setup --------------------------------------------------------------

# Find Python code from the root directory
import os
import sys
sys.path.insert(0, os.path.abspath('..'))


# -- Project information -----------------------------------------------------

project = 'CCP'
copyright = '2020, Mattéo Delabre'
author = 'Mattéo Delabre'


# -- General configuration ---------------------------------------------------

def linkcode_resolve(domain, info):
    """Resolve a Python object to the line number where it is defined."""
    if domain != 'py':
        return None

    # Resolve the module name to an the corresponding module object
    mod = sys.modules.get(info['module'])

    if mod is None:
        return None

    # Find the relevant Python object in that module
    obj = mod

    for part in info['fullname'].split('.'):
        try:
            obj = getattr(obj, part)
        except AttributeError:
            return None

    # Find the source file and line number of that object
    try:
        filename = os.path.relpath(
            inspect.getsourcefile(obj),
            os.getcwd())
    except TypeError:
        return None

    try:
        lineno = inspect.getsourcelines(obj)[1]
    except OSError:
        lineno = None

    if lineno:
        linespec = f'#L{lineno}'
    else:
        linespec = ''

    base_url = 'https://gitlab.inria.fr/mdelabre/ccp/-/blob/master/'
    return f'{base_url}{filename}{linespec}'


extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.linkcode',
    'sphinx.ext.imgmath',
]


# -- Options for HTML output -------------------------------------------------

html_theme = 'alabaster'
html_static_path = ['_static']

# -- Options for LaTeX formulas ----------------------------------------------

imgmath_font_size = 14
