*************************
Concurrent Control Phases
*************************

This is a set of tools aimed at achieving a better understanding of the *Concurrent Control Phases* (CCP) model through simulation and experimentation.
The CCP model builds upon MAPE-K loops, which lie at the foundation of autonomous systems, to introduce concurrency on the *Plan* and *Execute* phases so as to reduce the response time to events.

.. contents:: Table of contents
    :local:
    :backlinks: none

Simulator
=========
.. py:module:: ccp.simulator

(:py:mod:`ccp.simulator` module.)

Using the `SimPy <https://simpy.readthedocs.io/en/latest/>`_ discrete-event simulation framework, this central module provides a way to simulate the behavior of the CCP model when some changes occur in a distributed application deployed on an infrastructure composed of a set of hosts.

Concepts
--------

**Applications** (or “apps” for short) are sets of components that each need to be deployed (i.e. installed and running) somewhere on the target infrastructure.
Each component has a number of resource requirements expressed as abstract quantities.
Dependencies among components of an app are not taken into account for the time being.

**Infrastructures** (or “infras” for short) are sets of hosts able to contain and execute application components.
Each host has a given quantity of available resources which may be used by components.
The sum of requirements on a given resource from all the components running on a given host can never exceed the available quantity of this resource.
Network links between hosts of an infrastructure are neglected for the time being.

In a run of the simulation, there is one application which has to be deployed on one infrastructure.
Changes to either of those may happen at any time of the run.
For example, a new application component may be added, one of the existing hosts may become unavailable (i.e. removed) or its available resources may evolve.
In most cases, a new placement of the application on the infrastructure must be computed so that all components can eventually be up and running.
The *Plan* and *Execute* phases of the MAPE-K loop—and, by extension, of the CCP model—aim at performing these kinds of actions: collectively, they are referred to as the **reaction process**.
Each app or infra change must eventually be handled by a reaction process.
One of the metrics of interest is the time that it takes for each change to be successfully processed.

.. autofunction:: ccp.simulator.runner.run

Model
-----

Apps and infras are both modelled by `Dict[str,List[int]]`, i.e. Python dictionaries mapping strings to list of numbers.
For applications, each `str,List[int]` pair respectively represents each component’s name and list of required resources.
For infrastructures, they represent each host’s name and list of available resources.

Each change that may occur to apps and infras is modelled by an instance of the :py:class:`.change.Change` class.

Placements of components on hosts are modelled by `Dict[str,str]` of which keys are the names of components and values the names of hosts where those components are to be deployed.
These placements are theoretical deployment goals and do not reflect the actual deployment state of each component.
This state is rather modelled by instances of the :py:class:`.deployment.Deployment` which more finely describe whether a component is undeployed, in the process of being deployed, or currently deployed on a given host.

Finally, the :py:class:`.simulation.Simulation` class ties everything together by keeping track of the simulation’s current state at any point in time of a run.

.. autoclass:: ccp.simulator.change.Change
    :members:
    :special-members: __init__

.. autoclass:: ccp.simulator.deployment.Deployment
    :members:
    :special-members: __init__

.. autoclass:: ccp.simulator.simulation.Simulation
    :members:
    :special-members: __init__

.. _event-log-output:

Event log output
----------------

During a simulation run, a log of events aimed at further analysis of the run is produced.
This log takes the form of a stream of JSON objects, one for each event, following the `JSON Lines <http://jsonlines.org/>`_ format, i.e. one object per line.
Each object/event follows this shape:

.. list-table::
    :widths: 20 80
    :header-rows: 1

    * - Key
      - Description
    * - `time`
      - The simulated time at which the event occurred.
    * - `kind`
      - Type of event:

        * `change` events mirror the input list of changes that the app and infra go through.
        * `reaction` events report the state of a reaction process which follows a change.
        * `deployment` events report a change in the deployment state of a component.
    * - `value`
      - Additional event-specific information, in the form of a nested object.

        For `change` events (see :py:class:`.change.Change` for details):

        .. list-table::
            :widths: 20 80
            :header-rows: 1

            * - Key
              - Description
            * - `id`
              - An UUID4 for this change.
            * - `app`
              - New state of the application after the change.
            * - `infra`
              - New state of the infrastructure after the change.

        For `reaction` events (see :py:func:`.reactor.react` for details):

        .. list-table::
            :widths: 20 80
            :header-rows: 1

            * - Key
              - Description
            * - `id`
              - A shared UUID4 for all events from the same reaction process.
            * - `phase`
              - Current phase of the reaction process, either `plan` or `execute`.
            * - `kind`
              - More specific information on the type of the event.

        For `deployment` events (see :py:class:`.deployment.Deployment` for details):

        .. list-table::
            :widths: 20 80
            :header-rows: 1

            * - Key
              - Description
            * - `component`
              - Name of the affected component.
            * - `status`
              - Deployment status of the component, among `Ud`, `Dg`, `Dd`, `Mg` or `Ug`.
            * - `on`
              - If `status` is `Dd`, indicates where the component is deployed.
            * - `from`
              - If `status` is `Mg` or `Ug`, indicates the old host where the component was deployed.
            * - `to`
              - If `status` is `Dg` or `Mg`, indicates the target host on which the component will be deployed.

Reaction process
----------------

.. autofunction:: ccp.simulator.reactor.react
.. autofunction:: ccp.simulator.reactor.plan
.. autofunction:: ccp.simulator.reactor.execute

Placement algorithms
--------------------

Algorithms for application placement provide solutions to the problem of assigning each component to an host so as to satisfy a number of constraints and optimize a given criterion.
In this simulator, the constraints on placements are as follows:

* Each component must be assigned to exactly one host.
* Resources of each host must not be exceeded by the components which are assigned to it.
* The total number of migrations (i.e. the count of components that are already deployed on a host and will need to be moved to a new one) must not exceed the value of the `'allowed migrations'` simulation parameter.

A basic optimality criterion is also chosen: the total number of used hosts.

.. autofunction:: ccp.simulator.placements.mixed_integer_prog
.. autofunction:: ccp.simulator.placements.first_fit

Tester
======
.. py:module:: ccp.tester

(:py:mod:`ccp.tester` module.)

Tools for experimenting with the CCP model and analyzing simulation results are grouped together in this module.

Configuration generator
-----------------------

.. autofunction:: ccp.tester.generator.generate

.. autofunction:: ccp.tester.generator.make_random_var

.. autofunction:: ccp.tester.generator.make_random_action_generator

Metrics collection
------------------

.. autofunction:: ccp.tester.metrics.measure

Scripts
=======

(At the root of this repository.)

.. _scripts-simulate:

`./simulate`
------------

Load and simulate a CCP instance.

This script, a thin wrapper around the :py:func:`ccp.simulator.runner.run` function, loads a CCP instance from the JSON file it is given, runs it and sends the event log to the standard output.

Example:

.. code-block:: console

    > ./simulate examples/simple.json | tee log.jsonl
    {"time": 0, "kind": "change", "value": {"id": "65d5282a-fe87-41b9-bec1-a585b55c5d41", "app": {"A": [10, 1], "B": [20, 20], "C": [5, 30], "D"
    : [1, 2]}, "infra": {"1": [30, 20], "2": [20, 40], "3": [10, 10]}}}
    {"time": 0, "kind": "reaction", "value": {"id": "041d2790-4f18-4d47-b2b9-838d276fd7ed", "phase": "plan", "kind": "start"}}
    {"time": 5, "kind": "change", "value": {"id": "c25c19d7-dfc7-4dcd-9f82-e49f9369bb46", "app": {"A": [10, 1], "B": [20, 20], "C": [5, 30], "D"
    : [1, 2]}, "infra": {"2": [20, 40], "3": [10, 10], "4": [50, 50]}}}
    {"time": 5, "kind": "reaction", "value": {"id": "1288919b-cb20-48bc-9b63-df2131daed63", "phase": "plan", "kind": "start"}}
    {"time": 15, "kind": "reaction", "value": {"id": "041d2790-4f18-4d47-b2b9-838d276fd7ed", "phase": "plan", "kind": "end", "placement": {"A":
    "2", "C": "2", "D": "2"}}}
    ...

.. _scripts-measure:

`./measure`
-----------

Extract metrics from a run’s event log.

This script wraps around the :py:func:`ccp.tester.metrics.measure` function. It takes as an input an event log as generated by the :ref:`scripts-simulate` script and computes a number of metrics which are printed on the standard output.

Example:

.. code-block:: console

    > ./measure log.jsonl
    {"response": [0, 0, 0], "execution": [20, 30, 30]}

.. _scripts-viz:

`./viz`
-------

Create a visual representation of a run’s event log.

In this visualization, a time line, which runs at the bottom, represents the whole simulation run.
It is graduated in time units.
On this line, the sequence of changes that happened is represented as a series of hollow circles.
Each change is connected by an arrow to the first reaction process that handled it successfully.
Reaction processes, made up of *Plan*/*Execute* pairs, are represented as two-part rounded rectangles where the left parts are *Plan* phases (denoted with `P`) and the right parts are *Execute* phases (denoted with `E`).

The output of this script is a TikZ graphic which must be fed to LaTeX (e.g. to `pdflatex`) in order to produce a viewable representation.

Example:

.. code-block:: console

    > ./viz log.jsonl > viz.tex
    > mkdir -p viz-out
    > pdflatex --output-directory viz-out viz.tex
    > open viz-out/viz.pdf

.. figure:: viz.svg

    Visual representation of the execution log produced by running the `examples/simple.json` instance.
